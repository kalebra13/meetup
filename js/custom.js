//прикрепить навигацию к верхнему краю экрана
$(window).scroll(function() {
	if ($('.navi').offset().top > 105) {
		$('.navi').addClass("fixed-navi");
	} else {
		$('.navi').removeClass("fixed-navi");
	}
});

$(function () {
	//показать подробное описание события в расписании
	$('li.more').each(function() {
		$(this).find('span').click(function() {
			$(this).toggleClass('minus');
			$(this).parents('ul').siblings('.event-desc').slideToggle('slow');
		})
	});

	//обратный отсчёт
	$('#countdown').countdown({
			until: new Date(2018, 5, 10),
			padZeroes: true
			//layout: '{dn} {dl} {hn} {hl} {mn} {ml} {sn} {sl}'
		});
});
